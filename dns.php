<html>
	<head>
	</head>
	<body>
		<form action='#' method='post'>
			<input type='text' name='url' value='<?php echo $_POST['url']; ?>'/>
			<input type='text' name='rType' value='<?php echo $_POST['rType']; ?>' />
			<input type='submit' />
		</form>
	</body>
</html>

<?php
	class DNS{
		
		//variable for storing the original dns lookup.
		protected $baseinfo = array();
		
		public function __construct( $hostname ){
			$this->baseinfo = dns_get_record( $hostname, DNS_ALL );
			$this->parseBaseInfo();
		}
		
		
		//prints the $baseinfo member. Not very pretty
		public function printDNS(){
			
			echo "<pre>";
			print_r( $this->baseinfo );
			echo "</pre>";
			
		}
		
		// Prints the specified record type "A","CNAME",etc
		public function printRecord( $type ){
			
			// Handle case discrepencies, since all DNS record types are uppercase
			$rtype = strtoupper($type);
			
			if( isset($this->{$rtype}) ){
				echo "<pre>";
				print_r( $this->{$rtype} );
				echo "</pre>";
			}
			
		}
		
		// Test Function for debugging the object.. Pretty pathetic right now.
		public function testObj(){
			echo "<pre>";
			print_r( $this );
			echo "</pre>";
		}
		
		// Private utility function for populating the member variables 
		private function parseBaseInfo(){
			for($i = 0;$i < count($this->baseinfo); $i++){
				
				foreach($this->baseinfo[$i] as $option => $value){
					if(strcmp($option,"type") == 0){
						$this->{$value}[] = $this->baseinfo[$i];
					}
				}
				
			}
		}
		
	}
	
	$record = new DNS( $_POST['url'] );
	$record->printRecord( $_POST['rType'] );
?>